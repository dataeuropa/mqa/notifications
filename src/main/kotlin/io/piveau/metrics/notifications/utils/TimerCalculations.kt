@file:JvmName("TimerCalculations")
package io.piveau.metrics.notifications.utils


import org.slf4j.LoggerFactory
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Year
import java.time.temporal.ChronoUnit

private val piveauLog = LoggerFactory.getLogger("TimerCalculations")

/**
 * Calculate millis until next. Calculates time until next run in milliseconds
 *
 * @param now
 * @param timerUnit
 * @param timerCount
 * @param runHour
 * @return
 */
fun calculateMillisUntilNext(
    now: LocalDateTime,
    timerUnit: TimerUnit,
    timerCount: List<Int>,
    runHour: Int
): Long {


    val runToday = now.hour < runHour

    val rundate: LocalDateTime

    if (timerUnit == TimerUnit.WEEK) {
        val timerDays = timerCount.filter { it in 1..7 }

        if (timerDays.isEmpty()) {
            return 0
        }
        val today = now.dayOfWeek.value
        var nextDay = if (runToday) {
            timerDays.firstOrNull { it >= today }?.minus(today)
        } else {
            timerDays.firstOrNull { it > today }?.minus(today)
        }
        if (nextDay == null) {
            nextDay = timerDays.first() + (7 - today)
        }

        rundate = now.plusDays(nextDay.toLong()).withHour(runHour).withMinute(0)



    } else {
        val monthLength = now.month.length(Year.isLeap(now.year.toLong()))
        val timerDays = timerCount.filter { it in 1..monthLength }
        if (timerDays.isEmpty()) {
            return 0
        }

        var nextDay = if (runToday) {
            timerDays.firstOrNull { it >= now.dayOfMonth }
        } else {
            timerDays.firstOrNull { it > now.dayOfMonth }
        }

        if (nextDay == null) {
            nextDay = timerDays.first()
            rundate = now.plusMonths(1).withDayOfMonth(nextDay).withHour(runHour).withMinute(0)
        } else {
            rundate = now.withDayOfMonth(nextDay).withHour(runHour)
        }


    }



    return now.until(rundate, ChronoUnit.MILLIS)
}

/**
 * Calculate last run date. The date on which the last check should have run.
 * Might no be the real last run date,  e.g. when this service was not active/deployed/etc...
 *
 * @param now
 * @param timerUnit
 * @param timerCount
 * @param runHour
 * @return the time of the (theoretical) last run
 */
fun calculateLastDate(
    now: LocalDateTime,
    timerUnit: TimerUnit,
    timerCount: List<Int>,
    runHour: Int
): LocalDate? {

    // could it have been run today?
    val runToday = now.hour > runHour

    val lastRunDate: LocalDateTime

    if (timerUnit == TimerUnit.WEEK) {
        val timerDays = timerCount.filter { it in 1..7 }

        if (timerDays.isEmpty()) {
            return null
        }
        val today = now.dayOfWeek.value
        //if it could have already run today, get the highest date including today, else excluding today
        var lastDay = if (runToday) {
            timerDays.findLast{ it <= today }?.let { today.minus(it) }
        } else {
            timerDays.findLast { it < today }?.let { today.minus(it) }
        }
        if (lastDay == null) {
            lastDay =(7 + today) - timerDays.last()
        }

        lastRunDate = now.withHour(runHour).minusDays(lastDay.toLong())



    } else {
        val monthLength = now.month.length(Year.isLeap(now.year.toLong()))
        val timerDays = timerCount.filter { it in 1..monthLength }
        if (timerDays.isEmpty()) {
            return null
        }

        var lastDay = if (runToday) {
            timerDays.findLast { it <= now.dayOfMonth }
        } else {
            timerDays.findLast { it < now.dayOfMonth }
        }

        if (lastDay == null) {
            lastDay = timerDays.last()
            lastRunDate = now.minusMonths(1).withDayOfMonth(lastDay).withHour(runHour)
        } else {
            lastRunDate = now.withDayOfMonth(lastDay).withHour(runHour)
        }


    }

    return lastRunDate.toLocalDate()
}