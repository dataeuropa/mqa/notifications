package io.piveau.metrics.notifications

import io.piveau.dcatap.DCATAPUriSchema
import io.piveau.json.ConfigHelper.Companion.forConfig
import io.piveau.metrics.notifications.check.NotificationVerticle
import io.piveau.metrics.notifications.database.*
import io.piveau.metrics.notifications.utils.*
import io.piveau.security.ApiKeyAuthProvider
import io.piveau.utils.ConfigurableAssetHandler
import io.piveau.utils.CorsHelper
import io.vertx.config.ConfigRetriever
import io.vertx.core.DeploymentOptions
import io.vertx.core.Launcher
import io.vertx.core.eventbus.ReplyException
import io.vertx.core.http.HttpServerOptions
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.handler.APIKeyHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.ext.web.openapi.RouterBuilder
import io.vertx.ext.web.openapi.RouterBuilderOptions
import io.vertx.ext.web.validation.RequestParameters
import io.vertx.ext.web.validation.ValidationHandler
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import kotlinx.coroutines.launch
import org.apache.http.HttpStatus
import org.slf4j.LoggerFactory
import java.time.LocalDateTime
import kotlin.streams.asSequence


class MainVerticle : CoroutineVerticle() {

    private val log = LoggerFactory.getLogger(this.javaClass)

    override suspend fun start() {
        val allConfig = ConfigRetriever.create(vertx).config.await()
        log.debug("Config: ${allConfig.encodePrettily()}")
        val port: Int = allConfig.getInteger(ENV_PORT, DEFAULT_PORT)
        log.debug("apikey: ${allConfig.getString(ENV_APIKEY, "NO API KEY")}")
        log.debug("mail_server_config: ${allConfig.getString(ENV_MAIL_SERVER_CONFIG, "NO MAIL SERVER CONFIG")}")
        log.debug("piveau_dcatap_uri_schema: ${allConfig.getString(ENV_PIVEAU_DCATAP_SCHEMA_CONFIG, "NO PIVEAU DCATAP URI SCHEMA")}")



        val schemaConfig = forConfig(allConfig).forceJsonObject(ENV_PIVEAU_DCATAP_SCHEMA_CONFIG)
        if (schemaConfig.isEmpty) {
            log.error("No schema config found")
            schemaConfig.put(
                "baseUri",
                allConfig.getString(
                    ENV_BASE_URI,
                    DCATAPUriSchema.DEFAULT_BASE_URI
                )
            )
        }
        DCATAPUriSchema.config = schemaConfig

        vertx.eventBus().registerDefaultCodec(GetAllMessage::class.java, GetAllMessageCodec())
        vertx.eventBus().registerDefaultCodec(OneCatalogueMessage::class.java, OneCatalogueMessageCodec())
        vertx.eventBus().registerDefaultCodec(ReplaceOrInsertOneMessage::class.java, ReplaceOrInsertOneMessageCodec())
        vertx.eventBus().registerDefaultCodec(BlacklistMessage::class.java, BlacklistMessageCodec())

        vertx.deployVerticle(
            DatabaseVerticle(),
            DeploymentOptions().setWorker(true).setConfig(allConfig)
        ).await()

        vertx.deployVerticle(
            NotificationVerticle(),
            DeploymentOptions().setWorker(true).setConfig(allConfig)
        ).await()
        log.info("verticles deployed")
        makeTimer(allConfig)

        val routerBuilder = RouterBuilder.create(vertx, "webroot/openapi.yaml").await()

        routerBuilder.options = RouterBuilderOptions().setMountNotImplementedHandler(true)

        val corsDomains: JsonArray = forConfig(allConfig).forceJsonArray(ENV_CORS_DOMAINS)
        if (!corsDomains.isEmpty) {
            CorsHelper(corsDomains).addRootHandler(routerBuilder)
        } else {
            log.info("no CORS domains configured")
        }

        routerBuilder.securityHandler(
            "ApiKeyAuth",
            APIKeyHandler.create(ApiKeyAuthProvider(allConfig.getString(ENV_APIKEY, "apiKey"))).header("Authorization")
        )

        routerBuilder.operation("healthCheck").handler(this::handleHealth)
        routerBuilder.operation("unsubscribeFromCatalogue").handler(this::handleUnsubscribe)
        routerBuilder.operation("subscribeToCatalogue").handler(this::handleSubscribe)
        routerBuilder.operation("checkNow").handler(this::handleCheckNow)
        routerBuilder.operation("checkNowSingle").handler(this::handleCheckNowSingle)

        val webClient = WebClient.create(vertx)
        val router = routerBuilder.createRouter()

        router.route("/images/favicon").handler(
            ConfigurableAssetHandler(
                allConfig.getString(
                    "PIVEAU_FAVICON_PATH",
                    "webroot/images/favicon.png"
                ), webClient
            )
        )
        router.route("/images/logo").handler(
            ConfigurableAssetHandler(
                allConfig.getString("PIVEAU_LOGO_PATH", "webroot/images/logo.png"),
                webClient
            )
        )

        router.route("/*").handler(StaticHandler.create())
        vertx.createHttpServer(HttpServerOptions().setPort(port)).requestHandler(router).listen().await()
        log.info("Server successfully launched on port [{}]", port)
    }

    /**
     * Handle health should represent the system status
     *
     * @param context
     */
    private fun handleHealth(context: RoutingContext) {
        //todo: check more things
        val message = JsonObject()
            .put("status", "OK")
        context.response().setStatusCode(200).end(message.encodePrettily())
    }

    private fun handleCheckNow(context: RoutingContext) {
        launch {
            val params: RequestParameters = context.get(ValidationHandler.REQUEST_CONTEXT_KEY)
            val param = params.queryParameter("lastScore").integer

            val message = JsonObject()
                .put("date", LocalDateTime.now().toString())

            if(param!=null){
                message.put("lastScore", param)
            }

            vertx.eventBus().send(NotificationVerticle.ADDRESS_NOTIFY_CHECK,
                message
            )
        }
        context.response().setStatusCode(HttpStatus.SC_ACCEPTED).end("accepted")
    }

    private fun handleCheckNowSingle(context: RoutingContext) {
        launch {
            val params: RequestParameters = context.get(ValidationHandler.REQUEST_CONTEXT_KEY)
            val catID =params.pathParameter("catalogueId").string
            val comparedScore = params.queryParameter("comparedScore")?.integer
            val mailreceiver = params.queryParameter("mailreceiver")?.string

            val message = JsonObject()
                .put("date", LocalDateTime.now().toString())

            if(comparedScore!=null&& comparedScore>=0){
                message.put("comparedScore", comparedScore)
            }
            if (catID!=null){
                message.put("catalogue", catID)
            }
            if (mailreceiver!=null){
                message.put("mailreceiver", mailreceiver)
            }

            vertx.eventBus().send(NotificationVerticle.ADDRESS_NOTIFY_CHECK,
                message
            )
        }
        context.response().setStatusCode(HttpStatus.SC_ACCEPTED).end("accepted")
    }

    /**
     * Handle unsubscribe is called, when a user should be blacklisted for a specific catalogue
     *
     * @param context
     */
    private fun handleUnsubscribe(context: RoutingContext) {
        blacklistAddRemove(context, DatabaseVerticle.ADDRESS_DB_BLACKLIST_ADD)
    }

    /**
     * Handle subscribe is called, when a user should be removed from the blacklist of a specific catalogue
     *
     * @param context
     */
    private fun handleSubscribe(context: RoutingContext) {

        blacklistAddRemove(context, DatabaseVerticle.ADDRESS_DB_BLACKLIST_REMOVE)

    }


    /**
     * Blacklist add remove wraps the subscribe/usubsribe logic, which uses mostly the same arguments
     *
     * @param context
     * @param address
     */
    private fun blacklistAddRemove(context: RoutingContext, address: String) {


        val catalogue = context.pathParam("catalogue")
        val mail = context.pathParam("mail")
        if (catalogue == null || mail == null) {
            context.response().setStatusCode(400).end("Missing Parameters")
            return
        }


        val message = BlacklistMessage(catalogue, mail)

        vertx.eventBus().request<JsonObject>(address, message) {
            if (it.succeeded()) {
                context.response().setStatusCode(200).end(
                    it.result().body().encodePrettily()
                )
            } else {
                it.cause().let { cause ->
                    if (cause is ReplyException) {
                        context.response().setStatusCode(cause.failureCode()).end(cause.message)
                    } else {
                        context.response().setStatusCode(500).end(cause.message)
                    }
                }
            }
        }
    }


    /**
     * Make timer collects the timer variables from the environment and hands them over to startTimer()
     *
     * @param config the apllications configuration Json Object
     */
    private fun makeTimer(config: JsonObject) {

        val timerUnit = TimerUnit.valueOf(config.getString(ENV_TIMER_UNIT, DEFAULT_TIMER_UNIT))

        val timerCount =
            config.getString(ENV_TIMER_COUNT, DEFAULT_TIMER_COUNT).split(",").stream().map { it.toIntOrNull() }
                .asSequence().filterNotNull().toList()


        log.info("TimerCount: $timerCount")

        val runHour = config.getInteger(ENV_TIMER_HOUR, DEFAULT_TIMER_HOUR)

        startTimer(timerUnit, timerCount, runHour)
    }

    /**
     * Start timer starts a timer for the next run, after the timer finished the next runtime will be calculated
     * and a new timer starts
     *
     * @param now
     * @param timerUnit a TimerUnit, like WEEK or MONTH
     * @param timerCount when in that TimerUnit the timer should run
     * @param runHour the hour of the day the timer should run
     */
    private fun startTimer(
        timerUnit: TimerUnit,
        timerCount: List<Int>,
        runHour: Int
    ) {
        val now = LocalDateTime.now()
        val next = calculateMillisUntilNext(now, timerUnit, timerCount, runHour)

        if (next == 0L) {
            log.error("Do not have next RunDate, cancel execution")
            return
        }

        vertx.setTimer(next) {
            //let it run
            log.info("Timer fired")
            vertx.eventBus().send(NotificationVerticle.ADDRESS_NOTIFY_CHECK, JsonObject().put("date",now.toString()))
            //start timer for next time
            //now will be that time, when the timer is set, that is either at the last runDate or service startup
            startTimer(timerUnit, timerCount, runHour)
        }
        log.info("Timer set to $next milliseconds")

    }


}

fun main(args: Array<String>) {
    Launcher.executeCommand("run", *(args.plus(MainVerticle::class.java.name)))
}

