package io.piveau.metrics.notifications


import io.piveau.metrics.notifications.utils.TimerUnit
import io.piveau.metrics.notifications.utils.calculateMillisUntilNext
import io.vertx.core.Vertx

import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.commons.logging.LoggerFactory
import java.time.LocalDateTime
import java.time.Month


@DisplayName("Timer Calculation methods test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(VertxExtension::class)
class TimerCalculationsTest {
    private val log = LoggerFactory.getLogger(this.javaClass)

    @Test
    fun calculateMillisUntilNextTest(vertx: Vertx, testContext: VertxTestContext) {


        //Test WEEK

        //Today: wednesday, 10 am, Next: Mondays, 9am
        var now = LocalDateTime.of(2020, Month.DECEMBER, 9, 10, 0)
        var c = calculateMillisUntilNext(now, TimerUnit.WEEK, listOf(1), 9)
        assert(c == 428_400_000L)


        //Today: wednesday, 9 am, Next: Wednesdays, 10am
        now = LocalDateTime.of(2020, Month.DECEMBER, 9, 9, 0)
        c = calculateMillisUntilNext(now, TimerUnit.WEEK, listOf(3), 10)
        assert(c == 3_600_000L)

        //Today: wednesday, 10 am, Next: Wednesdays, 9am
        now = LocalDateTime.of(2020, Month.DECEMBER, 9, 10, 0)
        c = calculateMillisUntilNext(now, TimerUnit.WEEK, listOf(3), 9)
        assert(c == 601_200_000L)

        //Today: wednesday, 9 am, Next: Wednesdays, 9am (should wait 1 week)
        now = LocalDateTime.of(2020, Month.DECEMBER, 9, 9, 0)
        c = calculateMillisUntilNext(now, TimerUnit.WEEK, listOf(3), 9)
        assert(c == 604_800_000L)


        //Today: wednesday, 9:30 am, Next: Thursday, 9am (should return less than one day)
        now = LocalDateTime.of(2020, Month.DECEMBER, 9, 9, 30)
        c = calculateMillisUntilNext(now, TimerUnit.WEEK, listOf(4), 9)
        assert(c == 84_600_000L)

        //Today: wednesday, 9 am, Next: Mondays & Fridays, 10am
        now = LocalDateTime.of(2020, Month.DECEMBER, 9, 9, 0)
        c = calculateMillisUntilNext(now, TimerUnit.WEEK, listOf(1,5), 10)
        assert(c == 176_400_000L)

        //Test with empty list of next days
        now = LocalDateTime.of(2020, Month.DECEMBER, 9, 9, 0)
        c = calculateMillisUntilNext(now, TimerUnit.WEEK, listOf(), 10)
        assert(c == 0L) { "List of next days is empty, should return 0" }



        //Test MONTH

        //Today: Dec, 9th, 10 am, Next: 1st, 9am
        now = LocalDateTime.of(2020, Month.DECEMBER, 9, 10, 0)
        c = calculateMillisUntilNext(now, TimerUnit.MONTH, listOf(1), 9)
        assert(c == 1_983_600_000L)


        //Today: Dec, 9th, 9 am, Next: Dec, 9th, 10am
        now = LocalDateTime.of(2020, Month.DECEMBER, 9, 9, 0)
        c = calculateMillisUntilNext(now, TimerUnit.MONTH, listOf(9), 10)
        assert(c == 3_600_000L)

        //Today: Dec, 9th, 10 am, Next: 9th, 9am
        now = LocalDateTime.of(2020, Month.DECEMBER, 9, 10, 0)
        c = calculateMillisUntilNext(now, TimerUnit.MONTH, listOf(9), 9)
        assert(c == 2_674_800_000L)

        //Today: Dec, 9th, 9 am, Next: 9th, 9am
        now = LocalDateTime.of(2020, Month.DECEMBER, 9, 9, 0)
        c = calculateMillisUntilNext(now, TimerUnit.MONTH, listOf(9), 9)
        assert(c == 2_678_400_000L)


        //Today: Dec, 9th, 9 am, Next:1st & 16th, 10am
        now = LocalDateTime.of(2020, Month.DECEMBER, 9, 9, 0)
        c = calculateMillisUntilNext(now, TimerUnit.MONTH, listOf(1,16), 10)
        assert(c == 608_400_000L)

        //Test with empty list of next days
        now = LocalDateTime.of(2020, Month.DECEMBER, 9, 9, 0)
        c = calculateMillisUntilNext(now, TimerUnit.MONTH, listOf(), 10)
        assert(c == 0L) { "List of next days is empty, should return 0" }


        testContext.completeNow()
    }

}