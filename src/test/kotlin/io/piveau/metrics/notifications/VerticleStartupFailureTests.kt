package io.piveau.metrics.notifications

import io.piveau.metrics.notifications.database.DatabaseVerticle
import io.piveau.metrics.notifications.utils.DEFAULT_MONGODB_SERVER_HOST
import io.piveau.metrics.notifications.utils.ENV_MONGODB_SERVER_HOST
import io.piveau.metrics.notifications.utils.ENV_MONGODB_SERVER_PORT
import io.vertx.core.DeploymentOptions
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith


@DisplayName("Verticle Startup Failure Regression Tests")
@Disabled("Connection ist not tested during startup anymore")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(VertxExtension::class)
class VerticleStartupFailureTests {
    private val vertx: Vertx = Vertx.vertx()


    @Test
    fun DatabaseVerticleStartupFailure(vertxTestContext: VertxTestContext) {

        try {
            val host: String = DEFAULT_MONGODB_SERVER_HOST
            val port = 27030 // use non-default port to prevent conflicts when developing



            val testConfig: JsonObject = JsonObject()
                .put(ENV_MONGODB_SERVER_HOST, host)
                .put(ENV_MONGODB_SERVER_PORT, port)
            vertx.deployVerticle(
                DatabaseVerticle::class.java.name,
                DeploymentOptions().setConfig(testConfig)

            ){
                if(it.succeeded()){
                    vertxTestContext.failNow(Exception("Should throw an error when connection to db fails"))

                }else{
                    vertxTestContext.completeNow()
                }
            }


        } catch (e: Exception) {
            vertxTestContext.failNow(e)
        }


    }





}