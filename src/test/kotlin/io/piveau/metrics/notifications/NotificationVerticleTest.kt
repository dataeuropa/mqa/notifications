package io.piveau.metrics.notifications

import io.piveau.metrics.notifications.database.*
import io.piveau.metrics.notifications.utils.*
import io.vertx.core.DeploymentOptions
import io.vertx.core.Vertx
import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import io.vertx.kotlin.coroutines.CoroutineVerticle
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import kotlin.streams.asSequence


@DisplayName("Notification Verticle methods test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(VertxExtension::class)
class NotificationVerticleTest {

    private val vertx: Vertx = Vertx.vertx()



    class NotificationTestDatabaseVerticle : CoroutineVerticle() {

        val NOTIFICATION_TEST_SETTING = "test.notification.setting"
        val NOTIFICATION_TEST_RESET = "test.notification.reset"

        var metrics = vertx.fileSystem().readFileBlocking("currentMetrics.json").toJsonObject()
        var user = JsonObject().put("catalogue", "govdata").put("blacklist", JsonArray())

        /**
         * Start
         *
         */
        override suspend fun start() {

            vertx.eventBus().consumer(DatabaseVerticle.ADDRESS_GETALL_FROM_DB, this::handleGetAll)
            vertx.eventBus().consumer(DatabaseVerticle.ADDRESS_GETONE_FROM_DB, this::handleGetOne)
            vertx.eventBus().consumer(DatabaseVerticle.ADDRESS_REPLACE_OR_INSERT_IN_DB, this::handleReplaceOrInsert)


            vertx.eventBus().consumer(NOTIFICATION_TEST_SETTING, this::handleTestSettings)
            vertx.eventBus().consumer(NOTIFICATION_TEST_RESET, this::handleTestReset)

        }

        private var allLength =1

        private fun handleTestReset(message: Message<JsonObject>){

            metrics = vertx.fileSystem().readFileBlocking("currentMetrics.json").toJsonObject()

            user = JsonObject().put("catalogue", "govdata").put("blacklist", JsonArray())
        }



        private fun handleTestSettings(message: Message<JsonObject>){


            when(message.body().getString("command", "")){
                "allLength" -> allLength = message.body().getInteger("value")
                else-> {
                    print("could not decipher test setting message")
                }
            }


        }

        /**
         * Get all documents of a certain collection, User or Catalogue
         *
         * @param message
         */
        private fun handleGetAll(message: Message<GetAllMessage>) {


            if(allLength==1){

                message.reply(JsonArray().add(metrics))
            }else{

                val arr = JsonArray().apply {
                    add(metrics)

                    for(i in 2..allLength){

                        val source = "abcdefghijklmnopqrstuvwxyz"
                        val destID = java.util.Random().ints(6, 0, source.length)
                            .asSequence()
                            .map(source::get)
                            .joinToString("")

                        add(metrics.copy().getJsonObject("info").put("id", destID))
                    }
                }
                message.reply(arr)
            }


        }

        private fun handleGetOne(message: Message<OneCatalogueMessage>) {

            val body = message.body()
            val query = JsonObject().apply {
                if (body.collection == DBCollection.USER) {
                    put("catalogue", body.catalogue)
                }
                if (body.collection == DBCollection.CATALOGUE) {
                    put("info.id", body.catalogue)
                }

            }


        }



        /**
         * Replace or insert a single document
         *
         * @param message
         */
        private fun handleReplaceOrInsert(message: Message<ReplaceOrInsertOneMessage>) {


            val body = message.body()

            //piveauLog.info(body.encodePrettily())




        }


    }



    @BeforeAll
    fun setup(vertxTestContext: VertxTestContext): Unit {

        //register message codecs, this would normally happen in main verticle


        vertx.eventBus().registerDefaultCodec(GetAllMessage::class.java, GetAllMessageCodec())
        vertx.eventBus().registerDefaultCodec(OneCatalogueMessage::class.java, OneCatalogueMessageCodec())
        vertx.eventBus().registerDefaultCodec(ReplaceOrInsertOneMessage::class.java, ReplaceOrInsertOneMessageCodec())
        vertx.eventBus().registerDefaultCodec(BlacklistMessage::class.java, BlacklistMessageCodec())

        try {

            //TODO: mock cache endpoint
            //TODO: mock triplestore




            val testConfig: JsonObject = JsonObject()
                .put(ENV_METRICS_ADDRESS, "localhost")
                .put("PIVEAU_TRIPLESTORE_CONFIG", JsonObject())
            vertx.deployVerticle(
                DatabaseVerticle::class.java.name,
                DeploymentOptions().setConfig(testConfig),
                vertxTestContext.completing()
            )


        } catch (e: Exception) {
            vertxTestContext.failNow(e)
        }


    }


    //TODO: test when new score is higher-> save new score
    //TODO: test when new score is lower-> save new score & send notification
    //TODO: test when no old data
    //TODO: test when corrupt old data
    //TODO: test when no new data
    //TODO: test active catalogues setting
    //TODO: test production enabled setting
    //TODO: test score delta
    //TODO: test sender address setting

}